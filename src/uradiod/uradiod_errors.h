#include <stdint.h>

#ifndef _URADIOD_ERRORS_H
#define _URADIOD_ERRORS_H

#define _URADIOD_ERROR_NO_ERROR					0
#define _URADIOD_ERROR_INVALID_LOG_LEVEL		1
#define _URADIOD_ERROR_NULL_POINTER				2
#define _URADIOD_ERROR_CAN_NOT_ACCESS_LOG_FILE	3
#define _URADIOD_ERROR_RUNNING_SPOOLER			4
#define _URADIOD_ERROR_RUNNING_UC_AUDITOR		5

#define _MAX_ERROR_TITLE_LENGTH				100
#define _MAX_ERROR_DESCRIPTION_LENGTH		300

extern uint16_t uradiod_error_no;

extern const char* errors_titles[_MAX_ERROR_TITLE_LENGTH];

extern const char* errors_descriptions[_MAX_ERROR_DESCRIPTION_LENGTH];

struct uradiod_error
{
	char error_title[_MAX_ERROR_TITLE_LENGTH + 1];
	char error_description[_MAX_ERROR_DESCRIPTION_LENGTH + 1];
};

/**
 *
 * It frees the error struct
 *
 * error			->			struct uradiod_error*, error struct previously allocated
 */
void uradiod_error_free_error(struct uradiod_error* error);

/**
 *
 * Returns the error title and the error description of the error_no
 *
 * error_no			->			int, number of the error (see _URADIOD_ERROR_* macros in uradio_errors.h)
 *
 * Return			->			struct uradiod_error*, error title and error description
 */
struct uradiod_error* error_no_to_error_struct(int error_no);

/**
 *
 * It gets the error title and description formatted into a string
 *
 * error_no			->			int, number of the error (see _URADIOD_ERROR_* macros in uradio_errors.h)
 *
 * Return			->			char*, error string. NULL in case of an error occurred. See logs.
 *
 */
char* error_no_to_error(int error_no);

#endif