#ifndef _URADIO_LOG
#define _URADIO_LOG

#define _URADIO_DAEMON_LOG_FILE_PATH	"/var/log/uRadio/daemon.log"

enum URADIOD_LOG_LEVELS
{
	INFO, DEBUG, WARNING, ERROR
};

#define _URADIOD_MAX_LOG_LEVEL	ERROR

extern const char* log_levels_strings[];

/**
 *
 * It writes log to the log file and the stdout if specified
 * 
 * log_level			->			int, log level (see: URADIOD_LOG_LEVELS)
 * log_message			->			char*, log message to log
 * print_to_stdout		->			int, 1 = Print the log to stdout also | 0 = Do not print to stdout, It writes only in the file
 * log_file				->			char*, path of the log file. If an empty string is provided the default log file is used (see _URADIO_DAEMON_LOG_FILE_PATH)
 *
 */
void uradio_log(enum URADIOD_LOG_LEVELS log_level, char* log_message, int print_to_stdout, const char* log_file);

#endif