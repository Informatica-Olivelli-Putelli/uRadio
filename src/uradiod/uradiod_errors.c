#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "uradiod_errors.h"

uint16_t uradiod_error_no = _URADIOD_ERROR_NO_ERROR;

const char* errors_titles[_MAX_ERROR_TITLE_LENGTH] = {
	"Error no error",
	"Invalid log level",
	"Detected null pointer after memory allocation",
	"Can not access log file",
	"Error occurred trying to run the spooler",
	"Error occurred trying to run the uC auditor",
};

const char* errors_descriptions[_MAX_ERROR_DESCRIPTION_LENGTH] = {
	"No error occurred",
	"An invalid log level was used in log function",
	"Detected null pointer after memory allocation",
	"An error occurred while trying to access the log file. Check for read/write permissions on the log file.",
	"An error occurred while trying to run the spooler. Check other log messages to find issues.",
	"An error occurred while trying to run the microcontroller (uC) auditor need to communicate with the microcontroller. Check other log messages to find issues.",
};

/**
 *
 * Returns the error title and the error description of the error_no
 *
 * error_no			->			int, number of the error (see _URADIOD_ERROR_* macros in uradio_errors.h)
 *
 * Return			->			struct uradiod_error*, error title and error description
 */
struct uradiod_error* error_no_to_error_struct(int error_no){
	struct uradiod_error* error;

	error = malloc(sizeof(struct uradiod_error));
	if(!(error == NULL)){
		strncpy(error->error_title, errors_titles[error_no], strlen(errors_titles[error_no]) + 1);

		strncpy(error->error_description, errors_descriptions[error_no], strlen(errors_descriptions[error_no]) + 1);
	}

	return error;
}

/**
 *
 * It frees the error struct. This could be a macro but I don't know what to do...
 *
 * error			->			struct uradiod_error*, error struct previously allocated
 */
void uradiod_error_free_error(struct uradiod_error* error){
	if(error != NULL)	free(error);
}

/**
 *
 * It gets the error title and description formatted into a string
 *
 * error_no			->			int, number of the error (see _URADIOD_ERROR_* macros in uradio_errors.h)
 *
 * Return			->			char*, error string. NULL in case of an error occurred. See logs.
 *
 */
char* error_no_to_error(int error_no){
	struct uradiod_error* error;
	char *error_string;

	error_string = malloc(sizeof(char) * ((_MAX_ERROR_TITLE_LENGTH + _MAX_ERROR_DESCRIPTION_LENGTH + 4)));

	error = error_no_to_error_struct(error_no);
	if(error == NULL){
		uradiod_error_no = _URADIOD_ERROR_NULL_POINTER;
		strncat(error_string, errors_titles[_URADIOD_ERROR_NULL_POINTER], strlen(errors_titles[_URADIOD_ERROR_NULL_POINTER]));
	}else{
		uradiod_error_no = error_no;

		strncpy(error_string, error->error_title, strlen(error->error_title));
		strncat(error_string, ": ", 2);
		strncat(error_string, error->error_description, strlen(error->error_description));

		uradiod_error_free_error(error);
	}

	return error_string;
}