#include <stdio.h>

#include "uradiod_log.h"
#include "uradiod_errors.h"

const char* log_levels_strings[] = {
	"INFO", "DEBUG", "WARNING", "ERROR"
};

/**
 *
 * It writes log to the log file and the stdout if specified
 * 
 * log_level			->			int, log level (see: URADIOD_LOG_LEVELS)
 * log_message			->			char*, log message to log
 * print_to_stdout		->			int, 1 = Print the log to stdout also | 0 = Do not print to stdout, It writes only in the file
 * log_file				->			char*, path of the log file. If an empty string is provided the default log file is used (see _URADIO_DAEMON_LOG_FILE_PATH)
 *
 */
void uradio_log(enum URADIOD_LOG_LEVELS log_level, char* log_message, int print_to_stdout, const char* log_file){
	FILE* log_file_f;

	if(log_level < 0 || log_level > _URADIOD_MAX_LOG_LEVEL){
		fprintf(stderr, "%s -> %s: '%d'\n", log_levels_strings[ERROR], error_no_to_error(_URADIOD_ERROR_INVALID_LOG_LEVEL), log_level);
		return;
	}


	if(*log_file != '\0'){
	 	log_file_f = fopen(log_file, "a");
		if(log_file_f == NULL){
			fprintf(stderr, "%s -> %s Log file path: '%s'\n", log_levels_strings[ERROR], error_no_to_error(_URADIOD_ERROR_CAN_NOT_ACCESS_LOG_FILE), log_file);
		}else{
			fprintf(log_file_f, "%s -> %s\n", log_levels_strings[log_level], log_message);
			fclose(log_file_f);
		}
	}

	if(print_to_stdout)
		fprintf(stdout, "%s -> %s\n", log_levels_strings[log_level], log_message);
}