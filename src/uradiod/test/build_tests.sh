#!/bin/sh

echo "############ BUILDING TESTS ############"

LOG_PATH="/var/log/uRadio/"
LIB=".."

DEBUG=-g3
FLAGS="-Wall -Wunused-variable -Wreturn-type -Wint-conversion -Wdeprecated-declarations -Wmisleading-indentation -Wimplicit-function-declaration"

mkdir -p $LOG_PATH
chmod o+rw $LOG_PATH

number_of_tests=$( ls *_test.c | wc -l)

for test_file in $( ls *_test.c ); do
	number_of_test=$((number_of_builded_tests+1))

    printf "/ Building test "\'$test_file\'" ($number_of_test/$number_of_tests) \\ \n"

    test_object=${test_file%".c"}".o"

	gcc -g3 -iquote$LIB $test_file $LIB/uradiod_errors.c $LIB/uradiod_log.c -o $test_object

    rc=$?
    if [[ rc -ne 0 ]]; then
    	printf "Error occurred while building $test_file. Error code: $rc.\n"
    	break
    fi
    
    printf "Test "$test_object" correctly built.\n"

	chmod o+x $test_object
    
	number_of_builded_tests=$((number_of_builded_tests+1))
    
done
echo "Finished."