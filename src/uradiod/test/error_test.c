#include <stdlib.h>

#include "uradiod_errors.h"
#include "uradiod_log.h"

#define _TEST_NAME			"Error routine test (see uradiod_errors.c)"

int main(int argc, char const *argv[])
{
	uradio_log(INFO, "Starting test '"_TEST_NAME, 1, "/lol");

	return 0;
}