#include <stdlib.h>

#include "uradiod.h"
#include "uradiod_log.h"
#include "uradiod_errors.h"

int main(int argc, char const *argv[])
{
	uradio_log(INFO, "Starting uRadio daemon...", 1, _URADIO_DAEMON_LOG_FILE_PATH);

	if(uradiod_start_spooler() != _URADIOD_ERROR_NO_ERROR){
		uradio_log(ERROR, error_no_to_error(_URADIOD_ERROR_RUNNING_SPOOLER), 1, _URADIO_DAEMON_LOG_FILE_PATH);
		exit(uradiod_error_no);
	}

	if(uradiod_start_uc_auditor() != _URADIOD_ERROR_NO_ERROR){
		uradio_log(ERROR, error_no_to_error(_URADIOD_ERROR_RUNNING_UC_AUDITOR), 1, _URADIO_DAEMON_LOG_FILE_PATH);
		exit(uradiod_error_no);
	}
	
	return uradiod_error_no;
}