#ifndef _URADIOD
#define _URADIOD

/**
 *
 * It starts the spooler.
 * The spooler is needed to check for files in the spooler folder to find command to execute
 * like increase the volume or change the song in that case.
 * The spooler is executed after a fork is called to create a subprocess dedicated to manage this work.
 */
int uradiod_start_spooler();

/**
 *
 * It starts the uC auditor.
 * The uC auditor is needed to communicate with the microcontroller to check for actions
 * like increase the volume or go to next song.
 * The uC auditor is executed after a fork is called to create a subprocess dedicated to manage this work.
 *
 */
int uradiod_start_uc_auditor();

#endif